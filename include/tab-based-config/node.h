#ifndef _dimeona_tab_based_config_node_h_
#define _dimeona_tab_based_config_node_h_


#include <string>
#include <map>
#include <utility>


namespace Dimeona { namespace TabBasedConfig {
	
	
	template <typename Char = char>
	class Node
	{
	public:
		typedef Char char_type;
		typedef Node<char_type> node_type;
		typedef std::basic_string<char_type> string_type;
		
	private:
		typedef std::multimap<string_type, node_type> map_type;
		
	public:
		typedef typename map_type::const_iterator iterator_type;
	
		static const node_type& none()
			{
				static const node_type none_node;
				return none_node;
			}
		
		Node() = default;
		Node(const node_type &node) = default;
		Node(node_type &&node) = default;
		
		const string_type& value() const
			{ return _value; }
		operator const string_type& () const
			{ return _value; }
		
		const node_type& operator [] (const string_type &key) const
			{
				auto element = _children.find(key);
				if (element == _children.end())
					return none();
				else
					return element->second;
			}
		const node_type& operator / (const string_type &key) const
			{
				return operator [] (key);
			}
			
		void set_value(const string_type &value)
			{
				_value = value;
			}
		node_type& insert(const string_type &key)
			{
				return _children.insert(std::move(typename map_type::value_type(key, Node()))).second;
			}
			
		iterator_type begin() const
			{
				return _children.begin();
			}
		iterator_type end() const
			{
				return _children.end();
			}
		iterator_type begin(const string_type &key) const
			{
				return _children.lower_bound(key);
			}
		iterator_type end(const string_type &key) const
			{
				return _children.upper_bound(key);
			}
		
		bool operator == (const node_type &node) const
			{
				return this == &node;
			}
		bool operator != (const node_type &node) const
			{
				return this != &node;
			}
			
		operator bool() const
			{
				return operator != (none());
			}
		bool operator ! () const
			{
				return operator == (none());
			}
		
	private:
		std::string_type _value;
		std::multimap<string_type, node_type> _children;
	};
	
	
} } //namespace Dimeona::TabBasedConfig


template <typename Char>
bool operator == (const Dimeona::TabBasedConfig::Node<Char> &node, const std::basic_string<Char> &value)
{
	return node.value() == value;
}

template <typename Char>
bool operator == (const std::basic_string<Char> &value, const Dimeona::TabBasedConfig::Node<Char> &node)
{
	return node.value() == value;
}

template <typename Char>
bool operator != (const Dimeona::TabBasedConfig::Node<Char> &node, const std::basic_string<Char> &value)
{
	return node.value() != value;
}

template <typename Char>
bool operator != (const std::basic_string<Char> &value, const Dimeona::TabBasedConfig::Node<Char> &node)
{
	return node.value() != value;
}


#endif //_dimeona_tab_based_config_node_h_
