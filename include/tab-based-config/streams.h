#ifndef _dimeona_tab_based_config_streams_h_
#define _dimeona_tab_based_config_streams_h_


#include "node.h"

#include <iostream>
#include <fstream>


namespace Dimeona { namespace TabBasedConfig {
	
	
	template <typename Char> class ConfigTraits;
	
#define DIMEONA_TAB_BASED_CONFIG_CONFIG_TRAITS(type, prefix) \
	template <> \
	class ConfigTraits<type> \
	{ \
	public: \
		typedef type char_type; \
		typedef std::basic_string<char_type> string_type; \
		\
		constexpr static char_type tab = prefix##'\t'; \
		constexpr static char_type variable = prefix##'$'; \
		constexpr static char_type reference = prefix##'&'; \
		constexpr static char_type keyword = prefix##'@'; \
		\
		static const string_type& include() \
		{ \
			static const string_type include = prefix##"include"; \
		} \
		static const string_type& alias() \
		{ \
			static const string_type include = prefix##"alias"; \
		} \
		static const string_type& invoke() \
		{ \
			static const string_type include = prefix##"invoke"; \
		} \
		static const string_type& set() \
		{ \
			static const string_type include = prefix##"set "; \
		} \
	};
	
	
	DIMEONA_TAB_BASED_CONFIG_CONFIG_TRAITS(char, );
	DIMEONA_TAB_BASED_CONFIG_CONFIG_TRAITS(wchar_t, L);
	DIMEONA_TAB_BASED_CONFIG_CONFIG_TRAITS(char16_t, u);
	DIMEONA_TAB_BASED_CONFIG_CONFIG_TRAITS(char32_t, U);
	
	
} } //namespace Dimeona::TabBasedConfig




#endif //_dimeona_tab_based_config_streams_h_
